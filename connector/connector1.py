import mysql.connector
import pandas as pd

# Connecting mysql server
database = mysql.connector.connect(
    host="localhost",
    user="root",
    password='manager',
    database="incubyte")
# Read sql query
#df = pd.read_sql("SELECT * from patients",con=database)
# Remove duplicates using distinct keyword
#df = pd.read_sql("SELECT DISTINCT * from patients",con=database)
# Remove Null and empty rows
df = pd.read_sql("SELECT DISTINCT * from patients WHERE Dr_Name != 'Null' and Dr_Name !=''", con=database)
def countrywise_customer(country):
   data=df.loc[df['Country'] == country]
   print(data)
   data.to_csv('F:/basic/assignment/output/' + "Table_"+country+".csv")
   print("CSV File created")
countrywise_customer("UK")